#! /bin/bash

set -ex

ANDROID_NDK="~/Android/android-ndk-r16b"

BORINGSSL_GIT_URL="https://github.com/google/boringssl"
BORINGSSL_GIT_TAG="fips-20180730"

build() {
    TARGET_ARCH=$1
    BORINGSSL_DIR="boringssl_android_$TARGET_ARCH"

    git clone "$BORINGSSL_GIT_URL" "$BORINGSSL_DIR"
    cd "$BORINGSSL_DIR"
    git checkout "$BORINGSSL_GIT_TAG"

    mkdir -p build32
    cd build32

    cmake -DANDROID_ABI="$TARGET_ARCH" \
          -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
          -DANDROID_NATIVE_API_LEVEL=16 \
          -GNinja ..
    ninja
    cd "../.."
}


build "x86"
build "armeabi-v7a"
